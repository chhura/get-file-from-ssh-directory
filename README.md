# README #

This README would normally document whatever steps are necessary to get your application up and running.


# INSTRUCTIONS #

Import the project in your eclipse as an existing maven project.

Search for SftpSSH.java Run the main class, confirm the details then in the console type tail and hit enter.

If it is working correctly you will see below:

changing directory to /app/jboss-fuse/jboss-eap-6.4/standalone/log/sftp> tail
------In the tail section------
-------File exists deleting
--------File not exists creating
--------Now Reding from /app/jboss-fuse/jboss-eap-6.4/standalone/log/server.log ---Writing to: C:\bin\out.txt

Screen Shots:
 

 
 

I use bareTail software to do a tail on my out.txt file

Couple functionality that I will do in future don’t know when though lol:
1.	If you lose connection to your vagrant the code retries again and again until it get a connection. Once it gets a connection it ends with error “===Error days End” instead of this error it will re-establish the tail command.
2.	The code doesn’t keep track of the file size (out.txt) so when it reaches 1GB I have to stop and restart the SftSSH program so that notepad++ and baretail can function properly, instead in future the code will create backup file and reset the out.txt to 0bytes 😊
